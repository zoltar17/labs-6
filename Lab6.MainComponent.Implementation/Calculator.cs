﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab6.MainComponent.Contract;
using Lab6.Display.Contract;

namespace Lab6.MainComponent.Implementation
{
    public class Calculator:ICalculator
    {
        IDisplay display;
        public Calculator(IDisplay display)
        {
            this.display = display;
        }
        public Calculator()
        {

        }
        public double Add(double a, double b)
        {
            return a + b;
        //    display.ChangeText(result);
        }

        public double Sub(double a, double b)
        {
            return a - b;
            //string result = String.Format("{0} - {1} = {2}", a, b, a - b);
        //    display.ChangeText(result);
        }


        public void PushToScreen(string text)
        {
            display.Text = text;
        }


        public double Mul(double a, double b)
        {
            return a * b;
        }

        public double Div(double a, double b)
        {
            return a / b;
        }
    }
}
