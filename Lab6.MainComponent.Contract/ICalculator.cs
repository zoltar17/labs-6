﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab6.MainComponent.Contract
{
    public interface ICalculator
    {
        double Add(double a, double b);
        double Sub(double a, double b);
        double Mul(double a, double b);
        double Div(double a, double b);
        void PushToScreen(string text);
    }
}
