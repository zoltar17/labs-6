﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Lab6.MainComponent.Contract;
using Lab6.ControlPanel.Contract;
using System.Reflection;
namespace Lab6.ControlPanel.Implementation
{
    enum Operation
    {
        Add, Sub, Mul, Div, None
    }
    internal partial class DisplayForm : Window
    {
        public DisplayForm(ICalculator calculator)
        {
            this.InitializeComponent();
            this.calculator = calculator;
            symbol = null;
            var test =Assembly.GetAssembly(typeof(ICalculator)).GetName().Version.Major;
            if (Assembly.GetAssembly(typeof(ICalculator)).GetName().Version.Major <2)
            {
                ButtonDiv.Visibility = Visibility.Hidden;
                ButtonMul.Visibility = Visibility.Hidden;
            }
        }
        private double number1;
        private double number2;
        private string symbol;
        private bool isComma = false;
        private Operation operation=Operation.None;
        private void ButtonNum_Click(object sender, System.Windows.RoutedEventArgs e)
        {

            symbol+=(sender as Button).Content;
            calculator.PushToScreen(symbol);
        }
        private ICalculator calculator;
        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            SaveNumber();
            operation = Operation.Add;
        }
        private void SaveNumber()
        {
            if (operation != Operation.None)
                ButtonEqual_Click(this, null);

            if (Double.TryParse(symbol, out number1))
            {
                symbol = null;
                isComma = false;
            }
            calculator.PushToScreen(symbol);
        }
        private void ButtonSub_Click(object sender, RoutedEventArgs e)
        {
            SaveNumber();
            operation = Operation.Sub;
        }

        private void ButtonMul_Click(object sender, RoutedEventArgs e)
        {
            SaveNumber();
            operation = Operation.Mul;
        }

        private void ButtonDiv_Click(object sender, RoutedEventArgs e)
        {
            SaveNumber();
            operation = Operation.Div;
        }

        private void ButtonEqual_Click(object sender, RoutedEventArgs e)
        {
            if (operation == Operation.None)
            {
                number1 = 0.0;
                symbol = null;
                calculator.PushToScreen(symbol);
            }
            else if (operation == Operation.Add)
            {
                if (Double.TryParse(symbol, out number2))
                {

                    number1 = AddWrapper();
                    symbol = number1.ToString();
                }
                calculator.PushToScreen(symbol);
            }
            else if (operation == Operation.Sub)
            {
                if (Double.TryParse(symbol, out number2))
                {

                    number1 = SubWrapper();
                    symbol = number1.ToString();
                }
                calculator.PushToScreen(symbol);
            }
            else if (operation == Operation.Mul)
            {
                if (Double.TryParse(symbol, out number2))
                {

                    number1 = MulWrapper();
                    symbol = number1.ToString();
                }
                calculator.PushToScreen(symbol);
            }
            else if (operation == Operation.Div)
            {
                if (Double.TryParse(symbol, out number2))
                {

                    number1 = DivWrapper();
                    symbol = number1.ToString();
                }
                calculator.PushToScreen(symbol);
            }
            isComma = false;
            operation = Operation.None;
        }
        private double AddWrapper()
        {
            return calculator.Add(number1, number2);
        }

        private double SubWrapper()
        {
            return calculator.Sub(number1, number2);
        }
        private double MulWrapper()
        {
            return calculator.Mul(number1, number2);
        }
        private double DivWrapper()
        {
            return calculator.Div(number1, number2);
        }



        private void ButtonComma_Click(object sender, RoutedEventArgs e)
        {

            if (!isComma)
            {
                symbol += ",";
                calculator.PushToScreen(symbol);
                isComma = true;
            }
        }
    }
}
